FROM maven:3-jdk-8-alpine

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn package

FROM openjdk:8-jdk-alpine
COPY --from=0 /usr/src/app/target/*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
